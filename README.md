### Hi, there<img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="25" height="25" />
  <br />
  
Professional AV, VR and Full Stack developer with 10 years of experience specializing in developing and designing AR & VR websites and applications with a focus on client satisfaction.

My fields of expertise include:
<br/>
<br/>
● 3D Skill:
<br/>
✓ Three.js / Babylon.js / GSAP / WebGL / Metaverse
<br/>
<br/>
● Tools:
<br/>
✓ Unity / Blender / Unreal Engine
<br/>
<br/>
● Front end languages:
<br/>
✓ React / Redux / React hooks / React context
<br/>
✓ AngularJS / Angular, Vue.js / VueX / Vuetify
<br/>
✓ Next.js / Gatsby, D3.js / Chart.js / Tailwind CSS / Bulma / Material-UI / Bootstrap
<br/>
✓ HTML/HTML5, CSS/CSS3, Javascript / TypeScript
<br/>
<br/>
● Back end languages:
<br/>
✓ PHP / Laravel / Symfony / CodeIgniter / Yii / CakePHP / Zend 
<br/>
✓ Node.js / Express.js / Meteor.js / Hapi.js / GraphQL
<br/>
✓ Python / Django / Flask, Ruby / Ruby on Rails
<br/>
<br/>
● Blockchain:
<br/>
✓ Ethereum / Solana / Binance Smart Chain / Solidity
<br/>
✓ DeFi(AMM models, Yield Farming, Staking, Liquidity Pools), DEX, NFT
<br/>
✓ Rust, Web3.js, Ether.js, Remix, Truffle, Hardhat
<br/>
<br/>
● Databases:
✓ MySQL / PostgreSQL / MongoDB / Oracle

Contact me to discuss more.
  
 <div>
  <img align="right" alt="GIF" src="https://github.com/abhisheknaiidu/abhisheknaiidu/raw/master/code.gif" width="500" height="320" />
</div>

<br/>
<br/>
**Languages and Tools:**
<br/>
<code><img height="20" src="https://github.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/javascript/javascript.png?raw=true"></code>
<code><img height="20" src="https://github.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/vue/vue.png?raw=true"></code>
<code><img height="20" src="https://github.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/react/react.png?raw=true"></code>
<code><img height="20" src="https://github.com/github/explore/5c058a388828bb5fde0bcafd4bc867b5bb3f26f3/topics/graphql/graphql.png?raw=true"></code>
<code><img height="20" src="https://github.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/nodejs/nodejs.png?raw=true"></code>
<code><img height="20" src="https://github.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/cpp/cpp.png?raw=true"></code>
<code><img height="20" src="https://github.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/python/python.png?raw=true"></code>
<code><img height="20" src="https://github.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/mysql/mysql.png?raw=true"></code>
<code><img height="20" src="https://github.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/firebase/firebase.png?raw=true"></code>
<code><img height="20" src="https://github.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/git/git.png?raw=true"></code>


<br/>
<br/>
**My Favorites Techs:**

![](https://img.shields.io/badge/Network-BitCoin-informational?style=flat&logo=bitcoin&logoColor=white&color=3bac3a)
![](https://img.shields.io/badge/Network-Ethereum-informational?style=flat&logo=ethereum&logoColor=white&color=3bac3a)
![](https://img.shields.io/badge/Language-Solidity-informational?style=flat&logo=solidity&logoColor=white&color=3bac3a)
![](https://img.shields.io/badge/Token-ERC721-informational?style=flat&logo=erc721&logoColor=white&color=3bac3a)
![](https://img.shields.io/badge/Token-ERC1155-informational?style=flat&logo=erc1155&logoColor=white&color=3bac3a)
![](https://img.shields.io/badge/Token-ERC20-informational?style=flat&logo=erc20&logoColor=white&color=3bac3a)
![](https://img.shields.io/badge/Framework-React-informational?style=flat&logo=react&logoColor=white&color=3bac3a)
![](https://img.shields.io/badge/Framework-Vue-informational?style=flat&logo=vue.js&logoColor=white&color=3bac3a)
![](https://img.shields.io/badge/Framework-Angular-informational?style=flat&logo=angular&logoColor=white&color=3bac3a)
![](https://img.shields.io/badge/Language-JavaScript-informational?style=flat&logo=javascript&logoColor=white&color=3bac3a)
![](https://img.shields.io/badge/Language-TypeScript-informational?style=flat&logo=typescript&logoColor=white&color=3bac3a)
![](https://img.shields.io/badge/Language-PHP-informational?style=flat&logo=php&logoColor=white&color=3bac3a)
![](https://img.shields.io/badge/Language-Laravel-informational?style=flat&logo=laravel&logoColor=white&color=3bac3a)
![](https://img.shields.io/badge/CI/CD-Github_Action-informational?style=flat&logo=github&logoColor=white&color=3bac3a)
![](https://img.shields.io/badge/Database-PostgreSQL-informational?style=flat&logo=postgresql&logoColor=white&color=3bac3a)
![](https://img.shields.io/badge/Database-MySQL-informational?style=flat&logo=mysql&logoColor=white&color=3bac3a)
![](https://img.shields.io/badge/Database-MongoDB-informational?style=flat&logo=mongodb&logoColor=white&color=3bac3a)
![](https://img.shields.io/badge/Shell-Bash-informational?style=flat&logo=gnu-bash&logoColor=white&color=3bac3a)
![](https://img.shields.io/badge/Tools-Docker-informational?style=flat&logo=docker&logoColor=white&color=3bac3a)

<p>
<img align="left" src="https://visitor-badge.laobi.icu/badge?page_id=jwenjian.visitor-badge" />
<img align="right" src="https://img.shields.io/github/followers/RisingStar522?label=Follow&style=social" />
</p>

<br/>

[![My github activity graph](https://activity-graph.herokuapp.com/graph?username=RisingStar522&theme=github&count_private=true&area=true&hide_border=true)](https://activity-graph.herokuapp.com/graph?username=RisingStar522&theme=github&count_private=true)



